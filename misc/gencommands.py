#!/usr/bin/env python3

import re
import pdb
import pprint

print("""
from .fields import *

COMMAND_HEADERS = [ 'id', 'name', 'official_name', 'command', 'response', 'description' ]

COMMANDS = [
""")

commands = {}
with open('commands.txt') as f:
    for l in f:
        e = list(map(str.strip,l.split('|')))[1:]
        if not e: continue

        if not e[0]: continue
        if e[0][0] == '-': continue
        if e[0] == '**Cmd**': continue

        code = int(e[0][:2],base=16)
        name = e[1]
        description = e[2]
        data_bytes = e[3]

        if name in commands:
            print(f"Ooops {name}")
            pdb.set_trace()

        code_name = name[3:].lower()
        if re.search('^go',code_name):
            code_name = 'go_' + code_name[2:]

        elif re.search('^set',code_name):
            code_name = 'set_' + code_name[3:]

        elif re.search('^get',code_name):
            code_name = 'get_' + code_name[3:]


        if code == 43:
            data_bytes = f"[ STR('info') ]"
        elif code == 50:
            data_bytes = f"[ BYTES('goal') ]"
        elif code == 145:
            data_bytes = f"[ BYTES('version') ]"
        elif code == 96:
            data_bytes = f"[ STR('text') ]"
        elif code == 101:
            data_bytes = f"[ BYTES('status') ]"
        elif code == 229:
            data_bytes = f"[]"
        elif code == 146:
            data_bytes = f"[ BYTES('id') ]"
        elif code == 114:
            data_bytes = f"[ STR('error') ]"
        elif data_bytes == '3':
            data_bytes = f"[ INT('value'), UNIT('unit') ]"
        elif data_bytes == '0':
            data_bytes = f"[]"
        elif data_bytes == '1':
            data_bytes = f"[ BYTES('bytes')]"
        elif data_bytes == '2':
            data_bytes = f"[ BYTE('byte1'), BYTE('byte2') ]"
        elif data_bytes == '5':
            data_bytes = f"[ STR('info') ]"
        elif data_bytes == '1-10':
            data_bytes = f"[ STR('misc') ]"
        elif data_bytes == '?':
            data_bytes = f"[ BYTES('config') ]"
        else:
            print(data_bytes, code)
            break

        print("     [ {:>3}, {:20} {:25}".format(
            code,
            f"'{code_name}',",
            f"'{name}',",
        ))

        if re.search('^get', code_name):
            print("       [], # command")
        else:
            print("       {} # command".format(
                f"{data_bytes},",
            ))

        if re.search('^set', code_name):
            print("       [], # response")
        else:
            print("       {} # response ".format(
                f"{data_bytes},",
            ))
        description = description.replace('"',r'\"')
        print("       {} ],".format(
            f'"{description}"',
        ))
        print()


        commands[name] = {
                    'cmd': code,
                    'name': name,
                    'code_name': code_name,
                    'description': description,
                    'data_bytes': data_bytes,
                }

print("""]


COMMANDS_BY_ID = {}
COMMANDS_BY_NAME = {}
for c in COMMANDS:
    command = dict(zip(COMMAND_HEADERS, c))

    COMMANDS_BY_ID[command['id']] = command
    COMMANDS_BY_NAME[command['name']] = command
    COMMANDS_BY_NAME[command['official_name']] = command
        
""")

"""
**Cmd**
**Name**
**Description of Command**
**Data Bytes**
**Interpreted As**
**Allowed Slave States**
"""
