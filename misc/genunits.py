#!/usr/bin/env python3

import re
import pdb
import pprint

with open('units.txt') as f:
    for l in f:
        m = re.search('(?P<id>\d+)\s+(?P<units>.*)',l)
        if not m:
            continue
        unit_id = int(m.group('id'))
        unit_name = m.group('units').strip()
        if not unit_name:
            continue

        print("[ {:3}, {:20} ],".format(
                unit_id,
                f"'{unit_name.lower()}',",
              ))

