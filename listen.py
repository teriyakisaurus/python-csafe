#!/usr/bin/env python3

import serial
import struct
import sys
import time

from csafe.commands import COMMANDS, COMMANDS_BY_ID, TypeClass, COMMANDS_BY_NAME
from csafe.constants import *

class CommandMessage:
    def __init__(self, command, payload):
        self.command = command
        self.payload = payload

    def __str__(self):
        c = self.command
        return f"{c['name']}<{c['id']}> {self.payload}"

    def serialize(self):
        payload = b''
        for field in self.command['payload']:
            data = self.payload[field.name]
            payload += data.serialize()
        payload_size = len(payload)
        #buf = struct.pack('BB', self.command['id'], payload_size)
        buf = struct.pack('B', self.command['id'])
        return buf + payload

class Message:
    def __init__(self):
        self.commands = []

    def append(self, command):
        self.commands.append(command)

    def dump(self):
        for command in self.commands:
            print(f" - {command}")

class MessageResponse(Message):

    def __init__(self, data):
        super().__init__()
        if data:
            self.parse(data)

    def parse(self, data):
        """ Converts a binary from a csafe device into a useful
            datastructure
        """

        # Validate that we start with the start flag
        if not data[0] == START_FLAG:
            raise ParseError('Missing START FLAG')

        # And let's make sure that the packet is complete
        if not data[-1] == STOP_FLAG:
            raise ParseError('Missing STOP FLAG')

        # Then let's figure out what response data happens to be
        frame_checksum = data[-2]
        frame_contents = data[1:-2]

        # Calculate the checksum then verify
        calc_checksum = 0
        for ch in frame_contents:
            calc_checksum ^= int(ch)
        if calc_checksum != frame_checksum:
            raise ParseError('Checksum Failed')

        # Then let's figure out what response data happens to be
        status = frame_contents[0]
        data_structures = frame_contents[1:]

        # If there is data to go along with this packet, then
        # let's parse that out too
        i = 0

        self.status = status

        while i < len(data_structures):
            command_id = data_structures[i]
            data_bytes = data_structures[i+1]
            end_i = i+2+data_bytes
            data_packet = data_structures[i+2:end_i]
            i = end_i

            command = COMMANDS_BY_ID[command_id]
            name = command['name']

            payload = {}
            for t in command['payload']:
                v, data_packet = t.unpack( data_packet )
                payload[t.name] = v

            command_obj = CommandMessage(
                                command=command,
                                payload=payload
                            )

            self.append(command_obj)

        return self

class MessageCommand(Message):

    def serialize(self):
        """ Converts the current message into a serialized packet
            that can be sent over the wire to the target exercise machine
        """
        data = struct.pack("B", START_FLAG)
        checksum = 0
        for command in self.commands:
            command_data = command.serialize()
            for b in command_data:
                checksum ^= b
            print(command_data.hex())
            data += command_data
        data += struct.pack("BB", checksum, STOP_FLAG)
        return data

    def command(self, command_name, *args, **kwargs):
        """ Adds a command to the databse
        """

        if not command_name in COMMANDS_BY_NAME:
            raise Exception(f"Unknown command {command_name}")

        command = COMMANDS_BY_NAME[command_name]

        payload = {}
        valid_keys = {}
        args = list(args)
        for item in command['payload']:
            valid_keys[item.name] = item
            if args:
                payload[item.name] = item.ingest(args.pop(0))

        if args:
            raise Exception(f"More parameters than required passed to {command_name}")

        for k in valid_keys:
            if k not in kwargs:
                continue
            if k in payload:
                raise Exception(f"Duplicate key '{k}' passed to {command_name}")

            payload[name] = item.ingest(payload[k])

        command_obj = CommandMessage(
            command = command,
            payload = payload,
        )

        self.append(command_obj)

        return self

    def __getattr__(self, k):
        """ This makes it possible to use the CSAFE commands listed
            in the documentation to add messages to payload for serialization
            We support the more pythonic snake case naming but also the camel
            case naming noted in the protocol specification
        """
        if k in COMMANDS_BY_NAME:
            return lambda *a, **kw: self.command( k, *a, **kw )
        return object.__getattr__(self, k)

class ParseError(Exception):
    pass

class CSAFE:
    def __init__(self, dev, timeout=0.1):
        self.port = serial.Serial(dev, 9600, timeout=timeout)

    def packet_send(self, message):
        binary_packet = message.serialize()
        self.port.write(binary_packet)
        return binary_packet

    def packet_get(self):
        """ Fetches the next packet on the line
        """
        data = self.port.read(1024)
        if not data: return
        response = MessageResponse(data)
        return response

    def do_command(self, cmd, *a, **kw):
        """ Invokes a command from the COMMANDS table
        """
        m = MessageCommand().command(cmd, *a, **kw)
        self.packet_send(m)
        return self.packet_get()

    def get_status(self):
        res = self.do_command('get_status')
        if not res:
            return f"NO RESPONSE"
        status_id = int(res.status & 0xf)
        if status_id > len(STATUSES):
            return f"ST<{status_id}>"
        return f"{status_id} = {STATUSES[status_id]}"

    def __getattr__(self, k):
        if k in COMMANDS_BY_NAME:
            return lambda *a, **kw: self.do_command(k, *a, **kw)

        raise AttributeError(f'{k} does not exist in {self}')

'''

strings = [
    b'\xf1\x02\x02\xf2',
    b'\xf1\x03\x03\xf2',
    b'\xf1\x83\x92\x0500005!\xf2',
    b'\xf1\x05\x05\xf2',
    b'\xf1\x06\x06\xf2',
    b'\xf1\x05\xa5\x03\n\x001\x98\xf2',
    b'\xf1\x85\xa5\x03\n\x001\x18\xf2',
    b'\xf1\x05\xa5\x03\x0b\x001\x99\xf2',
    b'\xf1\x85\xa5\x03\x0b\x001\x19\xf2',
    b'\xf1\x05\xa5\x03\x0b\x001\x99\xf2',
    b'\xf1\x85\xa5\x03\x0b\x001\x19\xf2',
    b'\xf1\x05\xa5\x03\x0b\x001\x99\xf2',
    b'\xf1\x85\xa5\x03\x0b\x001\x19\xf2',
    b'\xf1\x05\xa5\x03\x0b\x001\x99\xf2',
    b'\xf1\x85\xa5\x03\x0b\x001\x19\xf2',
    b'\xf1\x05\xa5\x03\x0b\x001\x99\xf2',
    b'\xf1\x85\xa5\x03\x0b\x001\x19\xf2',
    b'\xf1\x05\xa5\x03\x0b\x001\x99\xf2',
    b'\xf1\x85\xa5\x03\x0c\x001\x1e\xf2',
    b'\xf1\x05\xa5\x03\x0c\x001\x9e\xf2',
    b'\xf1\x85\xa5\x03\x0c\x001\x1e\xf2',
    b'\xf1\x05\xa5\x03\x0c\x001\x9e\xf2',
    b'\xf1\x85\xa5\x03\r\x001\x1f\xf2',
    b'\xf1\x05\xa5\x03\r\x001\x9f\xf2',
    b'\xf1\x85\xa5\x03\r\x001\x1f\xf2',
    b'\xf1\x05\xa5\x03\r\x001\x9f\xf2',
    b'\xf1\x85\xa5\x03\r\x001\x1f\xf2',
    b'\xf1\x05\xa5\x03\x0e\x001\x9c\xf2',
    b'\xf1\x85\xa5\x03\x0e\x001\x1c\xf2',
    b'\xf1\x05\xa5\x03\x0e\x001\x9c\xf2',
    b'\xf1\x85\xa5\x03\x0e\x001\x1c\xf2',
    b'\xf1\x05\xa5\x03\x0e\x001\x9c\xf2',
    b'\xf1\x85\xa5\x03\x0e\x001\x1c\xf2',
    b'\xf1\x05\xa5\x03\x0e\x001\x9c\xf2',
    b'\xf1\x85\xa8\x03\x00\x00Ke\xf2',
    b'\xf1\x05\xa8\x03\x00\x00K\xe5\xf2',
    b'\xf1\x85\xa8\x03\x00\x00Ke\xf2',
    b'\xf1\x05\xa8\x03\x00\x00K\xe5\xf2',
]

for s in strings:
    message = MessageResponse(s)
    print(s.hex())
    message.dump()


m = MessageCommand()
m.go_idle()
print("GO IDLE", m.serialize().hex())
print("EXPECT: F18282F2")

m = MessageCommand()
m.set_speed( 50, '0.1 km/hour' )

serialized = m.serialize()
print(serialized.hex())
print(MessageResponse(serialized).dump())

'''

csafe = CSAFE('/dev/ttyUSB1')

def status():
    print("STATUS:", csafe.get_status())

def reset():
    csafe.reset()

def go_idle():
    print(csafe.go_idle())

def go_in_use():
    # Finally we can move into used state
    print(csafe.go_in_use())

def go_finished():
    print(csafe.go_finished())

def get_packet(iterations=100):
    # We then wait for someone to enter their user ID
    loop = 0
    while True:
        data = csafe.packet_get()
        if data:
            print("GOT PACKET", data.dump())
            break
        loop += 1
        if iterations and loop > iterations:
            print("FAILED OUT")
            break

def get_id():
    # We then have to get the user id
    print(csafe.get_id())

def grade(v=1.5):
    print(csafe.set_grade(int(v*10), 76).hex())

def speed(v=5.5):
    print(csafe.set_speed(int(v*10), 49).hex())

status()
print("reset")
reset()
status()
print("go_idle")
go_idle()
print("get_packet")
get_packet()
print("get_id")
get_id()
print("go_in_use")
go_in_use()

#print("Getting 100 packets")
#for i in range(50):
#    data = csafe.packet_get()
#    if data:
#        print("AFTER GO READY", data)
#print("Got 100 packets")

print("get_packet")
get_packet()

#print("Setting GRADE")
#print(csafe.set_grade(15, 76).hex())
#status()
print(csafe.set_speed(55, '0.1 km/hour').hex())
status()

import pdb;pdb.set_trace()




