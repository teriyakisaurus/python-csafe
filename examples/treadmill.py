#!/usr/bin/env python3

# This example requires the pyserial module to be installed
import serial

from csafe import Controller

# For the connection to the treadmill
transport = serial.Serial('/dev/ttyUSB0', 9600, timeout=2)

csafe = Controller(transport, debug=True)

#csafe.set_speed(25, '0.1 km/hour')
#csafe.set_grade(25, '0.1 % grade')
#sys.exit()

print()
csafe.reset()
print()
print(csafe.get_status())
print()
csafe.go_idle()
print()
csafe.get_packet()
print()
print(csafe.get_status())
print()
csafe.get_id()
print()
csafe.go_inuse()
print()


#print("Getting 100 packets")
#for i in range(100):
#    data = csafe.get_packet()
#    if data:
#        print("AFTER GO READY", data)
#print("Got 100 packets")

#print("Setting GRADE")
#print(csafe.set_grade(15, 76).hex())
#status()

csafe.set_speed(55, '0.1 km/hour')

import pdb;pdb.set_trace()


