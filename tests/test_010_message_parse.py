#!/usr/bin/python

from csafe.constants import *
from csafe.messages import *
from csafe.commands import *

def test_message_parse():

    # This is just a status IDLE response
    resp = MessageResponse(b'\xf1\x02\x02\xf2')
    assert resp
    assert resp.status_id == STATUS_IDLE

    # This is when we get the ID back
    resp = MessageResponse(b'\xf1\x83\x92\x0500005!\xf2')
    assert resp
    assert len(resp.commands) == 1
    command_obj = resp[0]

    assert command_obj.spec['name'] == 'get_id'
    assert command_obj == CMD_GETID

    assert command_obj.id == b'00005'

test_message_parse()

