#!/usr/bin/python

from csafe.constants import *
from csafe.messages import *
from csafe.commands import *

def test_message_generate():

    # Generate a basic command
    msg = MessageCommand()
    assert msg
    msg, cmd = msg.go_idle()
    assert cmd
    assert len(msg.commands) == 1

    assert msg[0] == cmd

    # Serialization will ensure we get the proper bytearray
    assert msg.serialize() == b'\xf1\x82\x82\xf2'

    # Let's create a bit more complex example by adding
    # another command
    msg, cmd = msg.set_speed(55, '0.1 km/hour')
    assert cmd
    assert len(msg.commands) == 2

    assert msg[1] == cmd

    # This actually creates a message packet with two commands
    assert msg.serialize() == b'\xf1\x82%\x037\x001\xa2\xf2'


test_message_generate()

