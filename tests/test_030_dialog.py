#!/usr/bin/python

from csafe import Controller

class MockTransport:
    def __init__(self):
        self.to_controller = []
        self.from_controller = []

    def write(self, data):
        print("WRITE:", data.hex())
        return self.from_controller.append(data)

    def last_sent(self):
        return self.from_controller.pop(0)

    def queue(self, data):
        self.to_controller.append(data)

    def read(self, i):
        print("READ:", self.to_controller[0].hex())
        return self.to_controller.pop(0)

def test_dialog():

    # Create our test transport
    transport = MockTransport()

    # Wire it up
    csafe = Controller(transport=transport)
    assert csafe

    # We are going to request the status from the device
    # so let's setup a reply
    transport.queue(b'\xf1\x02\x02\xf2')

    # Then let's send our first message to the transport
    resp = csafe.get_status()
    assert resp

    # Verify that we had sent our "what's up?" message
    assert transport.last_sent() == b'\xf1\x80\x80\xf2'

    # And let's check to see that the message we received says
    # that we are idle
    assert resp.status == 'idle'


test_dialog()


