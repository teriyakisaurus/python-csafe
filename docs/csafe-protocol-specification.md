

**1. CSAFE Physical Requirements**

**1.1 RS-232 serial interface description**

The Slave hardware must support an asynchronous communication mode of 9600 with 8 data bits, 1 stop bits and no parity. Hardware handshaking for flow control (CTS as an input to Slave) is not required to support this protocol but is recommended for future compatibility. See discussion under Frame Length heading to follow.

**1.2 Modular Connector Hookup**

The Slave's receptacle is a modular 8-pin (RJ-45) socket with the following connector pin assignments.

 

| **Pin** | **Description**  | **Fitness Equipment (Slave) I/O** |
| ------- | ---------------- | --------------------------------- |
| 1.      | Audio Left       | Input                             |
| 2.      | Audio Right      | Input                             |
| 3.      | Rx               | Input                             |
| 4.      | Tx               | Output                            |
| 5.      | Voltage source   | Output                            |
| 6.      | CTS Flow control | Input                             |
| 7.      | Signal Ground    | N/A                               |
| 8.      | Shield           | N/A                               |

 

Pin positions are counted from 1 to 8 in a left to right direction looking into the RJ-45 socket with the locking tab facing down as shown in the following diagram;

![img](https://web.archive.org/web/20071206133027im_/http://www.fitlinxx.com/CSAFE/socket.gif)

**Notes:**

(1) Voltage source requirements are to supply 4.75 V to 10.0 V nominal DC with I*MAX* (current) to sink (master) at 85mA.

(2) This voltage source pin may also be used as an RS-232 DTR output signal to tell the Master or network adapter that the Slave unit is powered on and operational.

(3) This connector configuration differs slightly from the original CSAFE specification to allow backward compatibility with some existing RJ-11 based products. 

(4) Any connection between Signal Ground and Shield is manufacturer-specific and should not be assumed.

**2. CSAFE Protocol Framework**

**2.1 Frame Based Protocol**

Communication between the Master and the Slave is accomplished with a simple frame protocol that is easy to implement on a micro-controller yet is quite robust and versatile. There are several restrictions that we have adopted that greatly simplify the control logic:

·     The Slave only speaks when responding to a specific request from the Master, except under specific, well-defined circumstances that are defined later in this document.

·     A frame contains a checksum so that the receiver can verify the integrity of the data it receives. A bad frame is ignored and the receiver **does not** reply with rejection. Neither are good frames acknowledged at the frame level. In other words, frames are not ACKed or NAKed.

This means that almost all of the logic that controls the interaction between the Master and the Slave is in the Master. The Slave communication logic is very simple.

 ***2.1.1 Standard Frame Structure\***

The Standard frame is defined as stream of bytes with the structure:

| Standard Start Flag | Frame Contents | Checksum | Stop Flag |
| ------------------- | -------------- | -------- | --------- |
|                     |                |          |           |

In this implementation we use the values:

| Standard Start Flag | F1 hex (or 11110001 binary)                                  |
| ------------------- | ------------------------------------------------------------ |
| Stop Flag           | F2 hex (or 11110010 binary)                                  |
| Checksum            | 1 byte XOR of all bytes in Frame Contents. When calculating the checksum for a frame, a starting value of 0 is used. |
| Frame Contents      | Binary bytes.. See below for discussion of maximum byte count. |

A frame that contains no data, called the "Empty Frame", may be transmitted and requires the use of the Start Flag, a Checksum byte of (00h), and Stop flag for proper operation.

***2.1.2 Byte-Stuffing\***

To make the receiving software simple, we must ensure that the unique Start and Stop flag bytes never appear in the Frame Contents or in the Checksum. If we do this, the detection of the Start or Stop flag in the byte stream has an unambiguous meaning, i.e. the beginning or the end of a frame. This is accomplished by a "byte-stuffing" technique that transforms any of the four bytes of the form 111100xx (binary), i.e. F0, F1,F2 and F3 hex, into the two byte sequence 11110011 000000xx. For example, the Start Flag, F1 hex, if found somewhere in the Frame Contents, would be converted into the two-byte sequence: F3, 01 hex when it was transmitted. Hence the Start and Stop Flag bytes can never occur in the final byte stream transmitted except at the beginning and end of the frame and we will have accomplished our goal of reserving the Start and Stop Flag values to appear only at the ends of the frame. 

If the "byte-stuffing" is done "on the fly," then the occasional extra byte inserted by the byte stuffing procedure will not effect the buffer size needed to hold a message. By "on the fly" we mean that the transmission software will, as each byte is fetched from the transmission buffer to be sent, examine it to see if it needs to be expanded into two bytes and if so transmit two bytes instead of one. The byte stuffing is undone by the receiving software before the received bytes are stored in the receive buffer. Thus the extra bytes inserted by the byte stuffing do not effect the size of the receive or transmission buffer needed. Note that the byte stuffing is done after the Checksum is calculated and is applied to the Checksum byte as well as the Frame Contents. 

The use of unique Start and Stop bytes make the frames "self-starting" in the sense that the beginning and end of a frame are determined by looking for an unique byte. When a full frame is received its integrity is determined from the checksum byte. If the Start byte or Stop byte is missed or the receiver begins receiving in the middle of a frame the same simple logic will discard the partial frame and correctly receive the next good frame **without** the need for any time based decisions.

***2.1.3  Frame Length\***

The frame definition itself does not place any limit on the number of data bytes that it can contain. There are, however, definite hardware restrictions since a frame that is being received must be received in its entirety before the checksum test is made to determine the integrity of the frame. Before the frame's integrity is determined its contents cannot be used hence must be buffered. This results in the following mandatory restrictions:

·     A Slave implementation must be able to buffer at **least** 16 bytes of received data exclusive of flags and checksum. It can, of course, have a larger buffer which would allow larger command sequences in a single frame. A larger buffer is not required for implementation of this protocol but may be useful in future CSAFE protocol implementations.

·     A Slave implementation can not transmit a frame larger than 32 bytes total (including flags, byte stuffing and checksum) unless CTS flow control is supported. Further, a Slave without CTS flow control cannot transmit a total of more than 32 bytes spread over more than one frame until it has received a request for more frames from the Master. This requirement is to prevent the Slave from overrunning the buffer of the network adapter. A corollary to this requirement is that the master must be able to buffer a minimum of 32 bytes and must provide flow control if used with Slave implementations that require flow control.

***2.1.4  Addressing and Extended Frames\***

The Basic frame definition does not contain any addressing information because in a Master-Slave relationship with no peer-to-peer communications, i.e. direct Slave to Slave, frames are going either from the Master to a particular Slave or from a Slave to the Master. The network that is transporting the frames handles the addressing. The Slave has no addressing functions. In other implementations such as peer-to-peer communications the Slave must tell the network where to send a frame and addressing has to be added to the protocol. This is discussed in the Peer-To-Peer section where the Extended Frame structure is defined. 

 

**2.2   Command and Response Structures**

The Frame Protocol and the Command and Response Structures are completely separate. The Frame Protocol is designed to transport data without regard to the meaning of the data; the only restrictions imposed on the data arise from the maximum frame length and the requirement that a frame not contain partial Command or Response structures. The Command and Response Structures describe the meaning of the information contained in the Frame Contents and do not concern the method of transport. 

***2.2.1  Command Structure\***

 All commands sent from the Master to the Slave have one of two structures -- the Long Command Structure for commands accompanied by data or the Short Command Structure for commands with no associated data.

 The Long Command Structure is:

| Long Command | Data Byte Count | Data |
| ------------ | --------------- | ---- |
|              |                 |      |

 

| Long Command    | A Long Command byte from the Command Table below. All Long Commands have their 7th bit off, i.e. range 00H to 7FH |
| --------------- | ------------------------------------------------------------ |
| Data Byte Count | Number of Data bytes for this command not counting either the Command byte or the Data Byte Count byte. Range 0-255. |
| Data            | Zero or more bytes of Data associated with this Command.     |

 

The Short Command Structure is:

 Short Command

 

| Short Command | A Short Command byte from the Command Table below. All Short Commands have their 7th bit on, i.e. range 80H to FFH. |
| ------------- | ------------------------------------------------------------ |
|               |                                                              |

 

One or more complete command structures can be in a single frame so long as the maximum allowed frame size is not exceeded. The meaning of the Data is defined by the specific command that precedes it.

The primary reason for the Data Byte Count in Long Commands is to allow new commands to be added to a system containing old Slave units that cannot execute the new commands. The CSAFE Protocol requires that a Command that is not recognized by a Slave unit be handled by ignoring the command and skipping over associated data using the Data Byte Count. 

***2.2.2 Response Structure\***

All responses that are sent from the Slave to the Master have the same structure called the Response Structure.

The Response Structure is:

| Status Byte | Zero or more Data Structures |
| ----------- | ---------------------------- |
|             |                              |

 

A Data Structure has the form:

| Identifier | Data Byte Count | Data |
| ---------- | --------------- | ---- |
|            |                 |      |

 

| Status Byte     | Gives the current status of the Slave. See diagram below.    |
| --------------- | ------------------------------------------------------------ |
| Identifier      | Identifies the following data bytes using the same Command byte requested by the Master. |
| Data Byte Count | Number of Data bytes for this identifier not counting either the identifier or the Data Byte Count byte. 1-255. |
| Data            | One or more bytes of Data associated with this Identifier.   |

 

Again, the primary reason for the Data Byte Count is to allow responses to be added to a system that cannot recognize them. The CSAFE Protocol requires that an unrecognized identifier be skipped over by using the Data Byte Count. 

**2.3 Transactions**

The typical implementation of the CSAFE protocol would have most transactions initiated by the Master and responded to by the Slave. The exceptions to the command-followed-by-response rule are the unsolicited announcement by the Slave of certain events. The events that trigger an unsolicited response by the Slave are selectable and include events such as a change in state of the Slave (e.g being powered up or finishing a workout) or commands sent from the Slave to control an external system.

The ability of the Slave to issue unsolicited response frames improves the response time of the Master to user activity on the Slave such as entering an ID. It will also reduce network traffic by removing the need for the Master to continually poll each Slave unit (even the units that are off) to determine if there has been a change in state of the Slave or some other pending situation that needs the attention of the Master.

There are, however, network implementations that would have difficulty with unsolicited responses by a Slave. This situation is handled by disabling the unsolicited response mechanism through the cmdAutoUpload command.

Transactions initiated by the Master fall into two general categories: those that request information from the Slave and those that pass information to the Slave.

It is possible that the Master may generate a frame containing only commands that require no response by the slave (for example, a frame containing only the cmdUpList command). The slave will not respond to such a frame even if it considers the frame in error (frame status not prevOK), but will just set the frame status value. It is possible for the Master to configure the Slave to respond to all frames, regardless of content, by enabling frame acknowledgement through the cmdAutoUpload command. If so configured, the Slave will respond to frames from the Master that would otherwise require no response by generating a Response Structure containing the Status Byte and no Data Structures.

Commands that request information from the Slave are responded to by a Response Structure that contains the Status Byte and the requested information in the form of Data Structures.

When the Slave changes state, the Slave can send an unsolicited Status Byte to the Master. The frame contains only the Status Byte. While this is the typical action of the Slave it can be disabled by the cmdAutoUpload command from the Master.

A command from the Master that should have produced a response and does not within a Time-out period will be sent again. There is no specified number of times the Master can retry a command. The Slave is required to be able to withstand an unlimited number of retries without exhibiting unacceptable behavior.

The Time-out period for a retry will not be less than 1 second but can be longer depending on circumstances such as traffic on the network.

Individual bytes that make up a Frame should not be separated by more than 1 second. There is no specified action that the Slave should take if the separation is larger and it will be left up to the individual implementations. If there is a choice, we recommend that no time-out be implemented since the frame protocol is self-starting and it is possible that occasionally the network traffic might result in a frame being delivered in two parts with a longer intervening delay. If the Slave times out, the frame would be resent by the Master with the only penalty being the retransmission delay.

**2.4 Flow Diagram**

From the point of view of the Master system the Slave will be treated as a state machine that accepts a specific set of commands and provides a set of specific responses that are determined by the current state of the Slave. The Slave switches between states based on 1) commands sent by the Master system, 2) actions taken by the current user of the Slave, and 3) events triggered by the elapse of certain periods of time. The following diagram illustrates the connections between the various states and the events that cause a change in state.

 

![img](https://web.archive.org/web/20071207110624im_/http://www.fitlinxx.com/CSAFE/Flow.jpg)

 

The Slave initializes to the Ready state. Therefore, a Master must be prepared for a transition to the Ready state at any time, in the event that the Slave is reset (e.g., power cycled by a user). It is also possible for the Slave to transition to the Error state (not shown on the diagram) at any time.

 

**2.5 The Status Byte**

The Slave sends the Status Byte as part of every frame sent to the Master. The Status Byte contains two types of information. The first is information about the integrity of the previous frame received from the Master and the results of processing it. The second informs the Master in a consistent manner of the current state of the Slave. The structure of the Status Byte is: 

| **Bits** | **Description**          |
| -------- | ------------------------ |
| 7        | Frame Count              |
| 6        | Reserved                 |
| 5-4      | Status of previous frame |
| 3-0      | State of Slave           |

 

***2.5.1  The Frame Count\***

The frame count is toggled by every frame received by the Slave that is OK (has status prevOK - see table below). The single exception is the "Empty Frame" command where by the Master can request the current Status of the Slave without changing any of the fields of the Status byte. The purpose of the exception is to allow the Master to examine the status of the Slave without changing the status, e.g. look at the Frame Count for error recovery without changing it.

The frame count may not change for any other types of frames, but only for frames with a prevOK status. It is therefore necessary that a slave fully process a received frame to determine that status before toggling the frame count.

If while processing a frame, the Slave changes state, and generates an unsolicited response frame (as configured by the AutoUpload byte), the unsolicited frame should contain the frame count value at the point in time when the unsolicited frame is generated. If the unsolicitied response frame is generated while the Slave is still processing the Master’s frame, that frame count value will not yet reflect the Master’s frame still being processed.

***2.5.2  The Status of Previous Frame\***

The Status of the Previous Frame informs the Master of what happened to last frame sent to the Slave.  

| **State** | **Name**   | **Description**                                              |
| --------- | ---------- | ------------------------------------------------------------ |
| 0         | prevOK     | Frame was processed without error.                           |
| 1         | prevReject | Frame was rejected because it was recognized as a legal frame (i.e. not prevBad) but contained a command that was illegal for the current state of the Slave or had illegal syntax.  This also includes commands considered illegal by the Slave – that is, recognized but not permitted. An unrecognized command with legal syntax is skipped - not rejected, |
| 2         | prevBad    | Frame had bad checksum or overran buffer. If the Slave can detect it, this status may also be used to indicate missing Start or Stop bytes. |
| 3         | prevNotRdy | Still processing last frame and cannot receive new frame at this time. NOTE: Many implementations will not be able to respond at all until the previous frame is processed and will simple discard incoming commands until the processing is complete. |

When a received frame contains an illegal command within multiple commands, a Slave implementation must decide how to handle commands beyond the failing command, as well as response information available from commands prior to the failing command.  A slave should choose one of the following two alternatives:

·     Upon receiving a complete frame, scan the frame for any illegal commands as if the frame were to be executed, including state transitions during that execution, but without actually executing the commands as they are scanned. If an illegal command is detected then set the prevReject status bit, but don’t take any action on any commands in the packet. This is the most accurate method, but is also more difficult to implement, as it requires a pre-scan phase that effectively simulates all commands and state changes.

·     Process the frame on a command-by-command basis. Upon reaching an illegal command, stop processing the remainder of the frame and set the prevReject status bit. If the commands processed prior to the illegal command generated response information, transmit any such pending information in a frame including the prevReject status. Only one response frame per received frame is allowed.

 

***2.5.3 State of the Slave\***

 

| **State** | **Name** | **Description**                                              |
| --------- | -------- | ------------------------------------------------------------ |
| 0         | Error    | Serious internal error of type that suggests unit not be used, e.g. unit has lost its calibration parameters. Slave should remain in this State until the problem has been fixed. |
| 1         | Ready    | The Initial state is entered when the Slave is turned on or is reset. The Slave remains in this state until either 1) a user begins a manual workout causing a jump to OffLine State, or 2) the Slave receives configuration commands from the Master and is promoted to Idle State. Once the Idle State is entered, the only way to get back to the Ready State is through a cmdGoReady command. |
| 2         | Idle     | Slave has been configured by the Master and is now part of the Network environment. The Slave is waiting for a user to enter an ID or a Start event (such as pressing a Start key without entering an ID). This is where a user chooses between 1) a Manual workout that is not monitored by the Master or 2) entering a valid ID to begin a Master sponsored workout. |
| 3         | HaveID   | A user ID or a Start event has been entered. The Master can request the ID and decide based on what was entered whether to issue a command to go the InUse State or back to Idle State. |
| 5         | InUse    | The user's workout program is running. If sufficient time elapses without activity or the user presses a pause button, the Pause State is entered. Finishing the workout jumps to the Finished state. |
| 6         | Paused   | The workout program is halted by the user. If sufficient time passes without the user restarting the program and returning to the InUse state, the Finished state will be entered. |
| 7         | Finished | The workout program is completed. The Master solicits the results of the workout and then issues the cmdGoIdle command to return to the Idle State. |
| 8         | Manual   | The user has elected a workout program that is not supervised by the Master. When he finishes the Slave will automatically return to the Idle state. |
| 9         | OffLine  | The user has begun a workout program and the Master has not configured the Slave. On finishing, the Slave will return to the Ready state. |

 

**3. CSAFE Protocol Commands**

 **3.1 Commands Issued by the Master**

Most communications actions, except the change-of-state upload of the Status Byte by the Slave discussed previously and certain automatic uploading of status and data discussed below, are begun by the Master and responded to by the Slave. The following tables contain the commands that the Master can issue. They are divided into three main groups.

·     The "Empty Frame" command that poll the Status of the Slave. The Slave responds with only a Status Byte. This command is unique in that it does not change the contents of the Status byte.

·     Commands that pass information to or command actions of the Slave. The Slave responds with a Status Byte (unless that feature is turned off).

·     Commands that request information from the Slave. The Slave's response will contain data as well as the Status Byte.

***3.1.1 The "Empty Frame" command by which the Master Requests Status from Slave\***

A frame that contains no commands is called the "Empty Frame" command. The Slave responds to an empty frame with only a Status Byte. This command does not change the Status, i.e. the Frame Count and the Status of the Previous Frame are not changed. The Slave will respond to an empty frame regardless of is current state. A non-response will be interpreted by the Master as indicating the Slave is powered down.

***3.1.2 Commands Issued by the Master that do not Request Data from Slave\***

By default, the Slave will not generate any response to these commands. If the flgAck flag is set on with the cmdAutoUpload command (the default is off), then the Slave will response to these commands with only a Status Byte.

 

3.1.2.1 Commands that Control the State of the Slave

| **Cmd** | **Name**       | **Description of Command**                                   | **Data Bytes** | **Interpreted As** | **Allowed Slave States**      |
| ------- | -------------- | ------------------------------------------------------------ | -------------- | ------------------ | ----------------------------- |
| 80H     | cmdGetStatus   | Request Status from Slave. Status is sent even if the flgAck flag is off, i.e. this command can be added to a frame to force an acknowledgment of the frame even it the flgAck is off. Unlike the "Empty Frame" this command does update the Status. | 0              | N/A                | all                           |
| 81H     | cmdReset       | Reset Slave. Initialize variables to Ready State and reset Frame Toggle and Status of Previous Frame flag to zero. | 0              | N/A                | Ready, Idle, HaveID, Finished |
| 82H     | cmdGoIdle      | go to Idle State, reset variables to Idle state              | 0              | N/A                | Ready, HaveID, Finished       |
| 83H     | cmdGoHaveID    | go to HaveID state                                           | 0              | N/A                | Idle                          |
| 85H     | cmdGoInUse     | go to InUse State                                            | 0              | N/A                | HaveID, Idle                  |
| 86H     | cmdGoFinished  | go to Finished State                                         | 0              | N/A                | InUse, Paused                 |
| 87H     | cmdGoReady     | go to Ready State                                            | 0              | N/A                | Ready, Idle, HaveID, Finished |
| 88H     | cmdBadID       | Indicates to Slave that the user ID entered was invalid      | 0              | N/A                | HaveID                        |
| 01H     | cmdAutoUpload  | Control automatic upload features. See table below for definition. | 1              | Byte (AutoUpload)  | Ready, Idle, InUse            |
| 02H     | cmdUpList      | List of commands that specify data for batched upload. *     | 1-10           | Bytes (commands)   | Ready, Idle                   |
| 04H     | cmdUpStatusSec | Interval between periodic automatic uploads of status **     | 1              | byte (seconds)     | all                           |
| 05H     | cmdUpListSec   | Interval between periodic automatic uploads of UpList **     | 1              | byte (seconds)     | all                           |

 \* cmdUpList contains a list of short commands that define a specific set of workout results. This construct allows the master to send a single cmdGetUplist command to receive a set of predefined workout results from the slave. Without this command the master would need to request each piece of workout results individually.

** Using a value of 0 for cmdUpStatusSec or cmdUpListSec is a request that the Slave continuously transmit the information as rapidly as possible. The actual frequency of updates is manufacturer and equipment-specific.

The AutoUpload byte controls the unsolicited upload by the Slave of status and data. Each bit of the AutoUpload Byte is a flag assigned to regulate a particular unsolicited upload feature. A value of 1 enables the feature and a 0 disables it. The default is disabled for all features except flgAutoStatus, which is enabled. The definitions of the flags are:

| **Bit** | **Name**         | **Default** | **Description of Flag**                                      |
| ------- | ---------------- | ----------- | ------------------------------------------------------------ |
| 0       | flgAutoStatus    | Enabled     | Unsolicited upload of Status on change of state of the Slave. |
| 1       | flgUpStatus      | Disabled    | Unsolicited upload of Status every UpStatusSec seconds (unless Status already uploaded in some other response) |
| 2       | flgUpList        | Disabled    | Unsolicited upload of UpList every UpListSec seconds.        |
| 4       | flgAck           | Disabled    | Every frame with a good checksum received from Master will be responded to by at least a Status byte (or a Status byte plus data if data was requested). |
| 6       | flgExternControl | Disabled    | Unsolicited upload of data used to control external systems (Commands in range C0h-CFh). |

 

3.1.2.2 Commands that Configure the Slave

These commands can only be issued in the Ready State.

| Cmd  | **Name**      | **Description of Command**                                   | **Data Bytes** | **Interpreted As** |
| ---- | ------------- | ------------------------------------------------------------ | -------------- | ------------------ |
| 10H  | cmdIDDigits   | An integer between 2 - 5 defining the number of digits to accept from the user as a valid ID | 1              | byte               |
| 11H  | cmdSetTIme    | Set current time of day                                      | 3              | Time*              |
| 12H  | cmdSetDate    | Set current date.                                            | 3              | Date*              |
| 13H  | cmdSetTimeout | Set timeout period for exiting certain states. See state diagram for details. | 1              | Seconds            |
| 1AH  | cmdUserCfg1   | Slave depended configuration information                     | ?              | Custom             |
| 1BH  | cmdUserCfg2   | Slave depended configuration information                     | ?              | Custom             |

\* See unit definitions below.

 3.1.2.3 Commands that Configure Workout Data

These commands permit a Master to configure Slave workout information, and can be used to implement appropriate per-user workout “presets”. Except as noted, these commands are available in the Ready, Idle or HaveID states. Slaves are permitted to restrict the commands accepted or states during which they accept some commands from this set (e.g., cmdSetSpeed) for safety or other reasons.

| **Cmd** | **Name**           | **Description of Command**            | **Data Bytes** | **Interpreted As**                                           | **Valid****Range (inclusive)**                               |
| ------- | ------------------ | ------------------------------------- | -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 20H     | cmdSetTWork        | Workout time goal                     | 3              | Time*                                                        | 0 - 255 for each byte                                        |
| 21H     | cmdSetHorizontal   | Horizontal distance goal              | 3              | Integer plus Unit* specifier                                 | 0 - 65,535                                                   |
| 22H     | cmdSetVertical     | Vertical distance goal                | 3              | Integer plus Unit* specifier                                 | 0 - 65,535                                                   |
| 23H     | cmdSetCalories     | Calories goal                         | 2              | Integer                                                      | 0 - 65,535                                                   |
| 24H     | cmdSetProgram      | Machine program and level             | 2              | Byte (program), Byte (level)                                 | 0 – 255                                                      |
| 25H     | cmdSetSpeed        | Equipment speed                       | 3              | Integer plus Unit* specifier                                 | 0 - 65,535                                                   |
| 28H     | cmdSetGrade        | Equipment grade (incline)             | 3              | Integer plus Unit* specifier                                 | 0 - 65,535                                                   |
| 29H     | cmdSetGear         | Equipment gear (resistance)           | 1              | Byte                                                         | 0 - 255                                                      |
| 2BH     | cmdSetUserInfo     | General user information              | 5              | Integer (weight) + Weight units* + Byte (age) + Byte (gender) | Weight (0-65,535) Age (0-255) Gender (0:None, 1:Male, 2:Female) |
| 2CH     | cmdSetTorque       | Equipment torque                      | 3              | Integer plus Unit* specifier                                 | 0 – 65,535                                                   |
| 2DH     | cmdSetLevel        | Level (bike=power, stepper=speed)     | 1              | Byte                                                         | 0 – 255                                                      |
| 30H     | cmdSetTargetHR     | Target HR (bpm)                       | 1              | Byte                                                         | 0-255                                                        |
| 32H     | cmdSetGoal         | Sets a workout goal01HTime in HR Zone | 1+?13          | By goalTime*                                                 | By goal0-255 for each byte                                   |
| 33H     | cmdSetMETS         | METS goal                             | 2              | Integer                                                      | 0 - 65,535                                                   |
| 34H     | cmdSetPower        | Power goal                            | 3              | Integer plus Unit* specifier                                 | 0 - 65,535                                                   |
| 35H     | cmdSetHRZone       | Target HR zone (bpm)                  | 2              | Byte (Min) + Byte (Max)                                      | 0 – 255                                                      |
| 36H     | cmdSetHRMax        | Maximum HR limit (bpm)                | 1              | Byte                                                         | 0 – 255                                                      |
| 40H     | cmdSetChannelRange | Audio channel range (inclusive)       | 2              | Byte (Low) + Byte (High)                                     | 0 – 255                                                      |
| 41H     | cmdSetVolumeRange  | Audio volume range (inclusive)        | 2              | Byte (Low) + Byte (High)                                     | 0 – 255                                                      |
| 42H     | cmdSetAudioMute    | Set audio muting state                | 1              | Byte                                                         | 0=not muted, 1=muted                                         |
| 43H     | cmdSetAudioChannel | Set audio channel                     | 1              | Byte                                                         | 0-255                                                        |
| 44H     | cmdSetAudioVolume  | Set audio volume                      | 1              | Byte                                                         | 0-255                                                        |

\* See unit definitions below.

1 The cmdSetGoal is a generic command used to permit the ongoing extension of further goal types without using additional primary commands from the command set. It is followed by a single byte representing the goal, and then data bytes specific to that goal as shown in the table.

3.1.2.4 Text Messaging Commands

These commands permit a Master to send/configure text banners to the Slave. Except as noted, these commands are available in the Ready, Idle, HaveID, InUse, Paused, and Finished states. Slaves are permitted to restrict the commands accepted or states during which they accept some commands from this set. The slave is in full control of presentation of the messages (length of time to display, delays, format, etc...). If at anytime the slave is reset, either on its own or in response to a CSAFE cmdReset command, the programmed text messages will be lost (reset to defaults).

| **Cmd** | **Name**             | **Description of Command**           | **Data Bytes** | **Interpreted As**                                           | **Valid****Range (inclusive)**       |
| ------- | -------------------- | ------------------------------------ | -------------- | ------------------------------------------------------------ | ------------------------------------ |
| 60H     | cmdStartText2,3,4,5  | Start text upload                    | 2+?            | Message type1, number of total frames of text to expect (may be 010), text to frame limit9 | 0 - 4 for message type0-255 for rest |
| 61H     | cmdAppendText2,3,4,5 | Append text to previous cmdStartText | ?              | Text to frame limit9                                         | 0 - 255                              |
| E0H     | cmdEndText2,6        | End of text upload                   | 0              | N/A                                                          | N/A                                  |
| E1H     | cmdDisplayPopup7     | Display pop up                       | 0              | N/A                                                          | N/A                                  |

Commands that request the state of text messaging on the slave:

| **Cmd** | **Name**           | **Data Bytes**    | **Description of Slave Response**                            | **Slave Data Bytes** | **Interpreted As**                                           | **Valid****Range (inclusive)** |
| ------- | ------------------ | ----------------- | ------------------------------------------------------------ | -------------------- | ------------------------------------------------------------ | ------------------------------ |
| 65H     | cmdGetTextStatus   | 1 (Message Type1) | Indicates the current status of a certain message type string on the slave | 1                    | 0=No text loaded for message type,1=Valid text loaded for message type,2=Text loaded for message type is invalid | 0 - 2                          |
| E5H     | cmdGetPopupStatus8 | N/A               | Indicates the status of a previous cmdDisplayPopup request   | 1                    | 0=Rejected 1=Pending 2=Displayed 3=No request received       | 0 - 3                          |

1 See text message type definitions below. A slave need not implement all text messaging types, see section 3.1.3.4, cmdGetCaps.

2 cmdStartText, cmdAppendText, and cmdEndText should be sent sequentially by the master and a query for status prior to cmdEndText is not permitted. If the slave receives any frames other than these or cmdGetStatus, it should reject any further text commands until another cmdStartText is seen.

3 If the slave has not received a cmdStartText prior to receiving a cmdAppendText or cmdEndText the slave should reject/ignore the frame, and set its status to prevReject

4 The master should never send text strings longer than max text string length returned by the slave in the capabilities query (see section 3.1.3.4). However, if the text string should overflow the slave at anytime the slave should reject/ignore any further text commands until another cmdStartText is seen.

5 It is recommended that the master check the slave status after every text frame upload. If the slave state is prevNotRdy or prevBad, the master may reattempt the frame. If the slave state is prevReject the master should abort the text upload and reattempt from the beginning.

6 If the slave has not received the total number of frames it expected prior to receiving a cmdEndText the slave should reject/ignore the cmdEndText frame, and set its status to prevReject

7 The slave is in full control of the display of the popup message (length of time to display, delays, format, etc...) and how the pop message is inserted into the ongoing status display on the console.

8 The slave will maintain the state of the last cmdDisplayPopup request by the master. The slave has several choices when the master sends a cmdDisplayPopup. It can reject the request outright, it can queue the request to be displayed later (if the display is busy), or it can display the message immediately. Please note a slave need not implement the pending of popup message, at minimum the slave can either display or reject the popup immediately. The state should be reset only when a new cmdDisplayPopup is received or the text for the popup message is changed with a cmdStartText with message type 4. A pending popup request should be canceled if the slave receives a new cmdDisplayPopup or the slave receives a cmdStartText with message type 4.

9 See section 3.1.3.4, cmdGetCaps for character set determination and frame limit. Note also that new lines in the text message are indicated by an embedded new line (0AH). The slave can choose to ignore newlines in its display of the message, it is only to provide optional enhanced formatting information to the slave.

10 The master may send an empty string (total frames of 0) to indicate to the slave to reset the indicated message type to the default message. The master must still send a cmdEndText after a cmdStartText with 0 total frames to expect. The slave should not reset the text message to the default state until a cmdEndText is received.

Message type definitions:

| **Type** | **Name**      | **Description of Type**                                      |
| -------- | ------------- | ------------------------------------------------------------ |
| 0        | Banner        | Slave adds this text to initial startup banner               |
| 1        | Enter ID      | Slave replaces "Enter ID" message with this text message. It will display this stored text message in the same manner as it would display the "Enter ID" message. |
| 2        | ID Accepted   | Slave replaces "ID Accepted" message with this message. It will display this stored text message in the same manner as it would display the "ID Accepted" message. |
| 3        | ID Rejected   | Slave replaces "ID Rejected" message with this message. It will display this stored text message in the same manner as it would display the "ID Rejected" message. |
| 4        | General Popup | Slave stores this message to be displayed later by cmdDisplayPopup |

 

***3.1.3 Commands Issued by the Master that Request Data from Slave\***

These commands request the Slave send data structures in addition to the Status byte. The data sent to the Master is identified by the Slave Identifier Byte. The “get” commands may be issued in any Slave state.

3.1.3.1 Commands that Request General Information from the Slave

These requests allow the Master to determine information about the type of Slave unit that is signing on.

| **Cmd** | **Name**           | **Description of Slave Response**                           | **Slave Data Bytes** | **Interpreted As**                         | **Valid Range (inclusive)** | **Allowed Slave States** |
| ------- | ------------------ | ----------------------------------------------------------- | -------------------- | ------------------------------------------ | --------------------------- | ------------------------ |
|         |                    |                                                             |                      |                                            |                             |                          |
| 91H     | cmdGetVersion      | Codes used to uniquely identify equipment and ROM version.2 | 5 + ?4               | Manufacturer, CID, Model, Version, Release | N/A                         | all                      |
| 92H     | cmdGetID           | ID # defined for the user                                   | 2-5                  | text                                       | 00000 – 99999               | all                      |
| 93H     | cmdGetUnits        | Unit Mode (Metric/English)                                  | 1                    | Logical                                    | 0-Metric1-English           | all                      |
| 94H     | cmdGetSerial       | Return equipment serial number                              | ?                    | Custom5                                    | Custom                      | all                      |
| 98H     | cmdGetList         | List of batched commands configured with cmdUpList.3        | ?                    | Custom                                     | Custom                      | all                      |
| 99H     | cmdGetUtilization  | Hours used since manufactured                               | 3                    | 3 byte integer                             | 0 to 16777215               | all                      |
| 9AH     | cmdGetMotorCurrent | Motor current                                               | 3                    | Integer plus Unit1 specifier               | 0-65,535                    | all                      |
| 9BH     | cmdGetOdometer     | Equipment odometer value                                    | 5                    | 4 byte integer plus Unit1 specifier        | 0 to 4294967295             | all                      |
| 9CH     | cmdGetErrorCode    | Equipment error code                                        | 3                    | 3 byte integer                             | 0 to 16777215               | all                      |
| 9DH     | cmdGetServiceCode  | Equipment service code                                      | 3                    | 3 byte integer                             | 0 to 16777215               | all                      |
| 9EH     | cmdGetUserCfg1     | Slave dependent configuration data                          | ?                    | Custom                                     | Custom                      | all                      |
| 9FH     | cmdGetUserCfg2     | Slave dependent configuration data                          | ?                    | Custom                                     | Custom                      | all                      |

1 See unit definitions below

2 Manufacturer and CID (CSAFE Class Identifier) bytes are defined by CSAFE; Release, Version, and Model bytes are defined by each manufacturer. Please contact us at [csafe@fitlinxx.com](https://web.archive.org/web/20071207110619/http://www.fitlinxx.com/CSAFE/csafe@fitlinxx.com) to have a manufacturer or CID code allocated.

3 It is illegal to request the UpList with cmdGetList without first programming it with cmdUpList.

4 The required 5 bytes in the cmdGetVersion response may be followed with manufacturer specific data (of any length subject to the CSAFE frame limitations). This data need not be interpreted by a Master, but can be logged to aid in further identification of the equipment.

5 It is suggested but not required that the serial number be returned as an ASCII encoding. The response also remains subject to CSAFE frame length limitations, which may in some circumstances require encodings other than ASCII.

 

3.1.3.2 Commands that Request Workout Data

These commands solicit the results of the workout.  These commands are available in any Slave State, reflecting either the current workout information (InUse, Paused) the previous completed workout (Finished), or manufacturer-specific values in other states. Some Slaves may continue to offer previous workout information in the Idle state, not re-initializing until HaveID or InUse, while others may re-initialize values upon entry to the Idle state.

| **Cmd** | **Name**         | **Description of Slave Response**                        | **Slave Data Bytes** | **Interpreted As**           | **Valid****Range (inclusive)** |
| ------- | ---------------- | -------------------------------------------------------- | -------------------- | ---------------------------- | ------------------------------ |
| A0H     | cmdGetTWork      | Workout duration ***                                     | 3                    | Time*                        | 0 - 255 for each byte          |
| A1H     | cmdGetHorizontal | Accumulated (for the workout) Distance (horizontal )     | 3                    | Integer plus Unit* specifier | 0 - 65,535                     |
| A2H     | cmdGetVertical   | Accumulated (for the workout) Distance (vertical)        | 3                    | Integer plus Unit* specifier | 0 - 65,535                     |
| A3H     | cmdGetCalories   | Accumulated Calories Burned                              | 2                    | Integer                      | 0 - 65,535                     |
| A4H     | cmdGetProgram    | Current Machine program and level                        | 2                    | Byte (program), Byte (level) | 0 - 255                        |
| A5H     | cmdGetSpeed      | Current Speed                                            | 3                    | Integer plus Unit* specifier | 0 - 65,535                     |
| A6H     | cmdGetPace       | Current Pace                                             | 3                    | Integer plus Unit* specifier | 0 - 65,535                     |
| A7H     | cmdGetCadence    | Current Cadence                                          | 3                    | Integer plus Unit* specifier | 0 - 65,535                     |
| A8H     | cmdGetGrade      | Current grade of exercise                                | 3                    | Integer plus Unit* specifier | 0 - 65,535                     |
| A9H     | cmdGetGear       | Current Gear                                             | 1                    | Byte                         | 0 - 255                        |
| AAH     | cmdGetUpList     | Batched workout results as defined by CmdUpList **       | ?                    | Custom                       | Custom                         |
| ABH     | cmdGetUserInfo   | Current user information                                 | 5                    | See cmdSetUserInfo           |                                |
| ACH     | cmdGetTorque     | Current torque                                           | 3                    | Integer plus Unit* specifier | 0 – 65,535                     |
| B0H     | cmdGetHRCur      | Current HR (BPM)                                         | 1                    | Byte                         | 0 - 255                        |
| B2H     | cmdGetHRTZone    | Current time in target HR zone                           | 3                    | Time*                        | 0 – 255 for each byte          |
| B3H     | cmdGetMETS       | Current 3.5 ml/kg/min oxygen consumption rating          | 2                    | Integer (0.1 METS units)     | 0 - 65,535                     |
| B4H     | cmdGetPower      | Current Power expenditure ( i.e. calories/min or watts ) | 3                    | Integer plus Unit* specifier | 0 - 65,535                     |
| B5H     | cmdGetHRAvg      | Current average heart rate (bpm)                         | 1                    | Byte                         | 0 – 255                        |
| B6H     | cmdGetHRMax      | Workout maximum heart rate (bpm)                         | 1                    | Byte                         | 0 - 255                        |
| BEH     | cmdGetUserData1  | Slave dependent workout data                             | ?                    | Custom                       | Custom                         |
| BFH     | cmdGetUserData2  | Slave dependent workout data                             | ?                    | Custom                       | Custom                         |

\* See unit definitions below.

 ** The Slave response to this command is a single frame containing a sequence of specific command identifiers and data bytes as defined by the cmdUpList configuration command. See example in Implementation Notes Section.

*** The inclusion of time outside of the main workout (e.g., warm-up or cool-down) in the cmdGetTWork response is manufacturer-specific.

3.1.3.3 Commands that retrieve External Control Signals from the Slave

These requests allow the Slave to send back control signals that are used to control or communicate with external systems (e.g. Cardio Theater). The Slave may be configured to automatically upload these messages using cmdAutoUpload with the flgExternControl bit enabled.

| **Cmd** | **Name**           | **Description of Slave Response** | **Slave Bytes** | **Interpreted As** | **Valid****Range (inclusive)** |
| ------- | ------------------ | --------------------------------- | --------------- | ------------------ | ------------------------------ |
| C0H     | cmdGetAudioChannel | Audio channel number selection    | 1               | byte               | 0-32                           |
| C1H     | cmdGetAudioVolume  | Audio volume setting              | 1               | byte               | 0-32                           |
| C2H     | cmdGetAudioMute    | Audio muting                      | 1               | Logical            | 0 - Not Muted1 - Muted         |

 

3.1.3.4 Commands that request slave capabilities

These requests allow the master to query the capabilities of the slave. As the CSAFE protocol becomes more complex, not all slaves will implement all the functionality in the protocol specification. This set of commands allows the master to query the slave to determine which functionality is supported, and dynamically adjust to the slave.

| **Cmd** | **Name**          | **Description**                                              | **Master Bytes**            | **Interpreted As** | **Valid Range** | **Description of Slave Response**                            |
| ------- | ----------------- | ------------------------------------------------------------ | --------------------------- | ------------------ | --------------- | ------------------------------------------------------------ |
| 70H     | cmdGetCaps        | Query capabilities of slave                                  | 1                           | Capability Code    | 0 - 3           | Description of slave capabilities based on master request. See following table of capability codes. |
| 71H     | cmdGetInfo        | Query equipment Information of slave                         | 1                           | Capability Code    | 0               | Description of slave information based on master request. See following table of capability codes. |
| 72H     | cmdGetErrorString | Returns a variable length printable ASCII string for associated error code. | 3+?, ErrorCode, ErrorString | 3 byte integer     | 0 to 16777215   | Master is requesting the ASCII string representation of the requested 3 byte error 8 |

Capability codes:

| **Caps Code** | **Name**        | **Description**                   | **Description of Slave Response**                            | **Slave Data Bytes** | **Interpreted As**                                           | **Valid Range**      |
| ------------- | --------------- | --------------------------------- | ------------------------------------------------------------ | -------------------- | ------------------------------------------------------------ | -------------------- |
| 0             | CSAFE Protocol  | Query CSAFE protocol parameters   | Maximum CSAFE RX and TX frame sizes                          | 34                   | Max RX frame, Max TX frame, Min Interframe Gap5              | 0 - 255              |
| 1             | CSAFE Power     | Query CSAFE power parameters      | Maximum power delivery on CSAFE port in mA @ specified voltage | 44                   | Integer Max Current(mA),Integer Rated Voltage(mV)            | 0 - 65,535           |
| 2             | Text            | Query slave text handling support | Text capabilities of the slave                               | 114                  | 2 bytes physical display size (width, height)2 bytes virtual display size (width, height)2 bytes max buffer space available, total space that all text strings can occupy on slave (integer 0 – 65,535) (0xFFFF= unlimited)2 bytes max text string length (integer 0 - 65,535)1 byte display type (0=fixed, 1=bus, 2=scroll)1 byte language map1,21 byte message type support (bitmask of types)3 | 0 - 255 unless noted |
| 3             | Command Support | Query slave for command support6  | Command and state support                                    | 3                    | 1 queried command2: lo byte state bit mask3: Hi byte state bit mask 7 | 0-255                |

 

1 The language map is an index of a defined (in the spec) 256-character encoding table that can be used for messages. For now only code 0 is valid. 

Code 0: standard ASCII 7-bit (20H-7FH) map

2 Maps may be partially supported; slave may replace any unsupported characters with a logical character of choice.

3 This is a bitmask of the message types defined in section 3.1.2.4 (command 60H). The bits are set from LSB to MSB, allowing for future expansion. Currently, 5 message types are defined therefore a slave supporting all message types will return 1FH. A slave that supports message types "banner" and "ID accepted" only, for example, will set bits 0 and 2 (00000101 or 05H).

4 Slave data bytes may expand with newer revisions of the specification. The master should parse only up to what it understands

5 Default time for inter-fame gap will be 0.1 seconds.

6 The size of the command structure for querying cmdGetCaps parameter 3(Query for slave support) is 4 bytes. The query format is ***cmdGetCaps 2 3 cmdId\***, where: ***2\*** is the size of the data structure, ***3\*** is the ***cmdGetCaps\*** capability code***, cmdId\*** is the command being queried for by the master.

7 The size of the response data structure for cmdGetCaps parameter 3 is 4 bytes. The four bytes include the command parameter 3, the command identifier being queried and a two-byte bitmask representation of the CSAFE states that support queried command. The Lo byte state bitpositions 0 -7 represents the CSAFE states 0-7. The Hi byte state bitpositions 0-7 representing CSAFE states 8-9.

8 The response format for cmdGetErrorString is variable length string that includes the three-byte error code and its associated ASCII string representation.

Information codes:

| **Caps Code** | **Name**       | **Description**                | **Description of Slave Response** | **Slave Data Bytes** | **Interpreted As**                                     | **Valid Range** |
| ------------- | -------------- | ------------------------------ | --------------------------------- | -------------------- | ------------------------------------------------------ | --------------- |
| 0             | Equipment Type | Query Slave for equipment type | Equipment type Identifier         | 1                    | 0 – None1 - Treadmill2 - Bike3 - Elliptical4 - Stepper |                 |

 

**3.2 Units and Other Conventions**

***3.2.1 General conventions\***

The general conventions used to specify quantities are:

·     All integers are sent low byte first and are considered unsigned integers unless otherwise specified.

·     Time is transferred as 3 bytes in HMS (H: hour, M: minute, S: second) binary format.

·     Date is transferred as 3 bytes in YMD (Y: year 0 - 255, M: month 1 - 12, D: day 1 - 31) binary format. Note that the year is added to 1900 hence can handle the new century.

***3.2.2 Units\***

Some of the commands and responses described above contain quantities whose meaning depends on the units being used. When there is more than one unit that could be used to express a quantity a extra byte has been added to the command or response structure to specify the correct unit. This method handles the case of the unit being changed during a workout. The following table gives the unit code for several standard units: 

| 1    | mile           | 33   | Km           | 65   | floors            | 97   | Amperes       |
| ---- | -------------- | ---- | ------------ | ---- | ----------------- | ---- | ------------- |
| 2    | 0.1mile        | 34   | 0.1km        | 66   | 0.1 floors        | 98   | 0.001 Amperes |
| 3    | 0.01 mile      | 35   | 0.01km       | 67   | steps             | 99   | Volts         |
| 4    | 0.001 mile     | 36   | Meter        | 68   | revolutions       | 100  | 0.001 Volts   |
| 5    | ft             | 37   | 0.1 meter    | 69   | strides           | 101  |               |
| 6    | inch           | 38   | Cm           | 70   | strokes           | 102  |               |
| 7    | lbs.           | 39   | Kg           | 71   | beats             | 103  |               |
| 8    | 0.1 lbs.       | 40   | 0.1 kg       | 72   | calories          | 104  |               |
| 9    |                | 41   |              | 73   | Kp                | 105  |               |
| 10   | 10 ft          | 42   |              | 74   | % grade           | 106  |               |
| 11   |                | 43   |              | 75   | 0.01 % grade      | 107  |               |
| 12   |                | 44   |              | 76   | 0.1 % grade       | 108  |               |
| 13   |                | 45   |              | 77   |                   | 109  |               |
| 14   |                | 46   |              | 78   |                   | 110  |               |
| 15   |                | 47   |              | 79   | 0.1 floors/minute | 111  |               |
| 16   | mile/hour      | 48   | Km/hour      | 80   | floors/minute     | 112  |               |
| 17   | 0.1 mile/hour  | 49   | 0.1Km/hour   | 81   | steps/minute      | 113  |               |
| 18   | 0.01 mile/hour | 50   | 0.01 Km/hour | 82   | revs/minute       | 114  |               |
| 19   | ft/minute      | 51   | Meter/minute | 83   | strides/minute    | 115  |               |
| 20   |                | 52   |              | 84   | stokes/minute     | 116  |               |
| 21   |                | 53   |              | 85   | beats/minute      | 117  |               |
| 22   |                | 54   |              | 86   | calories/minute   | 118  |               |
| 23   |                | 55   | Minutes/mile | 87   | calories/hour     | 119  |               |
| 24   |                | 56   | Minutes/km   | 88   | Watts             | 120  |               |
| 25   |                | 57   | Seconds/km   | 89   | Kpm               | 121  |               |
| 26   |                | 58   | Seconds/mile | 90   | Inch-Lb           | 122  |               |
| 27   |                | 59   |              | 91   | Foot-Lb           | 123  |               |
| 28   |                | 60   |              | 92   | Newton-Meters     | 124  |               |
| 29   |                | 61   |              | 93   |                   | 125  |               |
| 30   |                | 62   |              | 94   |                   | 126  |               |
| 31   |                | 63   |              | 95   |                   | 127  |               |
| 32   |                | 64   |              | 96   |                   | 128  |               |

**4. CSAFE Implementation Notes**

**4.1   General Implementation**

Except for certain minimum requirements, the manner in which the CSAFE Protocol is implemented on a particular model Slave is quite flexible. The Master server software can use a custom driver written for each Slave implementation thus allowing for the use of custom commands to handle idiosyncratic behavior or to increase efficiency.

It is expected that most Slave implementations will use only a small subset of the full array of commands available.

**4.2   Peer-To-Peer Considerations**

The Standard Frame is designed for a Master-Slave architecture in which the demands made on the Slaves are minimized and all network addressing issues are handled outside of the Slave. The Extended Frame protocol has an extension designed to accommodate addressing to support Slave to Slave communication for exercise activities that require information exchange between two or more Slaves or for simple multi-drop implementations of the supporting network. As with the Standard Frame, the Extended Frame definition is not concerned with the issue of how the frames are moved from device to device - that is the responsibility of the network - but rather to define a standard frame definition that provides:

·     a frame structure that allows the network to simply extract the destination address of the frame without having to know any details about the contents of the frame,

·     allows the receiver to extract from the frame the address of the sender.

An Extended Frame is defined as stream of bytes with the structure:  

| Extended Start Flag | Destination Address | Source Address | Frame Contents | Checksum | Stop Flag |
| ------------------- | ------------------- | -------------- | -------------- | -------- | --------- |
|                     |                     |                |                |          |           |

 In this implementation we use the values:

| Extended Start Flag | F0 hex (or 11110000 binary)                                  |
| ------------------- | ------------------------------------------------------------ |
| Stop Flag           | F2 hex (or 11110010 binary)                                  |
| Checksum            | 1 byte XOR of all bytes in Frame Contents                    |
| Destination Address | 1 byte address of intended recipient                         |
| Source Address      | 1 byte address of originator of frame                        |
| Frame Contents      | Binary bytes.. See below for discussion of maximum byte count. |

The Extended Frame is distinguished from the Standard Frame by its Extended Start Flag. Like the Standard Start Flag the Extended Start Flag only appears at the start of a frame due to the byte-stuffing used within the frame. As in the Standard Frame the entire contents of the frame except the Start and Stop flags are tested for byte-stuffing. 

The following rules apply:

·     Address 0 is reserved for the Master even if a Master does not exist.

·     Address FF hex matches all other addresses except 0, i.e. a frame with the Destination address FF hex is accepted by all units except the Master.

·     Address FE hex is reserved for expansion.

·     A frame with Destination address 0 will contain a Response Structure. A frame with any other Destination address will contain a Command Structure.

**4.3   Compatibility Requirements**

The Standard Frame core implementation must support the following:

·     Extended frames must be ignored if not implemented.

·     The Slave must respond correctly to the cmdGetVersion command with CID provided by CSAFE so that the Master will be able to remotely determine the kind of software driver that is required for this unit.

·     The Slave must respond correctly to an "Empty Frame" and cmdGetStatus commands.

·     The Slave must not transmit unsolicited data except as controlled by the AutoUpload byte. The cmdAutoUpload flags and associated functions for flgAutoStatus and flgAck must be implemented. The Slave should announce itself on the network when turned on. However it is desirable that the flgAutoStatus flag be configurable by jumper or in non-volatile memory since it is the only unsolicited response that occurs before the Master has a chance to disable unsolicited responses. Failure to allow for a non-volatile method of setting this flag could preclude the use of the Slave in a network that could not handle unsolicited responses.

·     Any command implemented must conform to the CSAFE definition. Exceptions should be implemented using one of the custom commands provided. Any use of a currently unused command or identifier could lead to incompatibilities with CSAFE in the future unless it is made part of the CSAFE protocol.

·     CTS flow control is not required for implementation of this protocol but may be useful in future CSAFE protocol extensions.

**4.4 Examples of CSAFE Transactions**

***4.4.1 Example of Transactions for a Complete Session\***

The following conversation was with a Slave that had the flgAutoStatus flag on. The Master requested accumulated Horizontal distance, Calories and Total Workout Duration from the slave upon workout completion.

 

| **Sender** | **Frame**                                             | **Description**                                              |
| ---------- | ----------------------------------------------------- | ------------------------------------------------------------ |
| Slave      | F1 01 01 F2                                           | Slave enters Ready state                                     |
| Master     | F1 91 02 03 A1 A3 A0 32 F2                            | Master requests Version info and programs UpList for later use |
| Slave      | F1 81 91 05 05 01 01 01 03 12 F2                      | Slave sends version info                                     |
| Master     | F1 82 82 F2                                           | Master commands Slave to Idle state.                         |
| Slave      | F1 02 02 F2                                           | Slave enters Idle state                                      |
| Slave      | F1 03 03 F2                                           | Slave received ID from user and enters HaveID state          |
| Master     | F1 92 92 F2                                           | Master requests the ID was that entered                      |
| Slave      | F1 83 92 05 30 31 32 33 34 35 15 F2                   | Slave returns ID (e.g., “12345”)                             |
| Master     | F1 85 85 F2                                           | Master verifies ID and instructs Slave to enter the InUse state |
| Slave      | F1 05 05 F2                                           | Slave enters InUse state                                     |
| Slave      | F1 07 07 F2                                           | Slave enters Finished state                                  |
| Master     | F1 AA AA F2                                           | Master request UpList                                        |
| Slave      | F1 87 A1 03 07 00 03 A3 02 0B 00 A0 03 00 00 30 18 F2 | Slave sends results as: Horizontal (0.07 miles), Calories (11 )and Workout Duration (48 sec). |
| Master     | F1 82 82 F2                                           | Master commands Slave to Idle state                          |
| Slave      | F1 02 02 F2                                           | Slave enters Idle state                                      |

 

***4.4.2 Example of Error Recovery\***

In this example the Master sends a frame with a bad checksum. When it doesn't receive the expected response it requests a Status report and re-sends the command. Note that this is not the only manner that the Master could have responded, e.g. it could have resent the command without requesting the Status.

 

| **Sender** | **Frame**   | **Description**                                   |
| ---------- | ----------- | ------------------------------------------------- |
| Master     | F1 82 82 F2 | Master commands Slave to Idle state.              |
| Slave      | F1 02 02 F2 | Slave enters Idle state                           |
| Master     | F1 80 81 F2 | Master requests Status but frame has bad checksum |
| Slave      |             | Slave does not respond                            |
| Master     | F1 00 F2    | Master sends Empty Frame requesting Status        |
| Slave      | F1 22 22 F2 | Slave responds with prevBad flag in Status        |
| Master     | F1 80 80 F2 | Master resends command                            |

 