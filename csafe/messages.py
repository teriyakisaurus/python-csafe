import struct

from .constants import *
from .exceptions import *
from .commands import Command, COMMANDS_BY_NAME
#COMMANDS, COMMANDS_BY_ID, TypeClass, COMMANDS_BY_NAME

class Message:
    def __init__(self):
        self.commands = []

    def append(self, command):
        self.commands.append(command)

    def dump(self):
        for command in self.commands:
            print(command)

    def __getitem__(self, k):
        if isinstance(k, int):
            return self.commands[k]
        return object.__getitem__(k)

    def __getattr__(self, k):
        if not len(self.commands):
            import traceback
            traceback.print_stack()
            raise CSAFEError(f"Message has no command elements")
        return getattr(self.commands[0], k)

    def serializable(self):
        """ Returns an easily serialized structure
        """
        message = {
            'commands': []
        }
        for command in self.commands:
            message['commands'].append(command.serializable())
        return message

class MessageResponse(Message):

    def __init__(self, data):
        super().__init__()
        if data:
            self.parse(data)

    def parse(self, data):
        """ Converts a binary from a csafe device into a useful
            datastructure
        """

        # Validate that we start with the start flag
        if not data[0] == START_FLAG:
            raise ParseError(f'Missing START FLAG {data}')

        # And let's make sure that the packet is complete
        if not data[-1] == STOP_FLAG:
            raise ParseError(f'Missing STOP FLAG {data}')

        # Then let's figure out what response data happens to be
        frame_checksum = data[-2]
        frame_contents = data[1:-2]

        # Calculate the checksum then verify
        calc_checksum = 0
        for ch in frame_contents:
            calc_checksum ^= int(ch)
        if calc_checksum != frame_checksum:
            raise ParseError('Checksum Failed')

        # Then let's figure out what response data happens to be
        status_id = frame_contents[0] & 0xf
        data_structures = frame_contents[1:]

        # If there is data to go along with this packet, then
        # let's parse that out too
        i = 0

        self.status_id = status_id
        self.status = STATUSES[status_id]

        while i < len(data_structures):
            command_id = data_structures[i]
            end_i = i+2+data_structures[i+1]
            data_packet = data_structures[i+2:end_i]
            i = end_i
            command_obj = Command.parse(command_id, data_packet)
            self.append(command_obj)

        return self

class MessageCommand(Message):

    def serialize(self):
        """ Converts the current message into a serialized packet
            that can be sent over the wire to the target exercise machine
        """
        data = struct.pack("B", START_FLAG)
        checksum = 0
        for command in self.commands:
            command_data = command.serialize()
            for b in command_data:
                checksum ^= b
            data += command_data
        data += struct.pack("BB", checksum, STOP_FLAG)
        return data

    def command(self, command_name, *args, **kwargs):
        """ Adds a command to the databse
        """

        if not command_name in COMMANDS_BY_NAME:
            raise Exception(f"Unknown command {command_name}")

        command = COMMANDS_BY_NAME[command_name]

        payload = {}
        valid_keys = {}
        args = list(args)
        for item in command['command']:
            valid_keys[item.name] = item
            if args:
                payload[item.name] = item.ingest(args.pop(0))

        if args:
            raise Exception(f"More parameters than required passed to {command_name}")

        for k in valid_keys:
            if k not in kwargs:
                continue
            if k in payload:
                raise Exception(f"Duplicate key '{k}' passed to {command_name}")

            payload[name] = item.ingest(payload[k])

        command_obj = Command( command, payload )

        self.append(command_obj)

        return self, command_obj

    def __getattr__(self, k):
        """ This makes it possible to use the CSAFE commands listed
            in the documentation to add messages to payload for serialization
            We support the more pythonic snake case naming but also the camel
            case naming noted in the protocol specification
        """
        if k in COMMANDS_BY_NAME:
            return lambda *a, **kw: self.command( k, *a, **kw )
        return object.__getattr__(self, k)


