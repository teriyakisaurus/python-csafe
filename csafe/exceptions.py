class CSAFEError(Exception):
    pass

class ParseError(CSAFEError):
    pass

