import struct
from csafe.units import UNITS_BY_ID, UNITS_BY_NAME

class Data:
    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return str(self.value)

    def serializable(self):
        return self.value

class DataByte(Data):
    def __init__(self, val):
        self.value = val

    def serialize(self):
        return struct.pack('B', self.value)

class DataInt(Data):
    def __init__(self, val):
        self.value = val

    def serialize(self):
        return struct.pack('H', self.value)

class DataUnit(Data):
    def __init__(self, val):

        if isinstance(val, int):
            if val not in UNITS_BY_ID:
                raise Exception(f"Unknown unit '{val}'")
            unit_name = UNITS_BY_ID[val]
            unit_id = val

        else:
            if val not in UNITS_BY_NAME:
                raise Exception(f"Unknown unit '{val}'")
            unit_name = val
            unit_id = UNITS_BY_NAME[val]

        self.value = unit_id
        self.unit_name = unit_name

    def __str__(self):
        return self.unit_name

    def __repr__(self):
        return self.unit_name

    def serialize(self):
        return struct.pack('B', self.value)

class DataBytes(Data):
    def __init__(self, val):
        self.value = val

class DataStr(Data):
    def __init__(self, val):
        self.value = val

class TypeClass:
    def __init__(self, name):
        self.name = name

    def ingest(self, value):
        return self._class(value)

    def serialize(self, value):
        pass

class BYTE(TypeClass):
    _class = DataByte
    def unpack(self, data):
        v = struct.unpack('B', data[:1])[0]
        return DataByte(v), data

class INT(TypeClass):
    _class = DataInt
    def unpack(self, data):
        v = struct.unpack('H', data[:2])[0]
        return DataInt(v), data[2:]

class UNIT(TypeClass):
    _class = DataUnit
    def unpack(self, data):
        v = struct.unpack('B', data[:1])[0]
        return DataUnit(v), data[1:]

    def ingest(self, value):
        return self._class(value)

class STR(TypeClass):
    _class = DataStr
    def unpack(self, data):
        v = struct.unpack('B', data[:1])[0]
        return DataUnit(v), data[1:]

class BYTES(TypeClass):
    _class = DataBytes
    def unpack(self, data):
        return data, b''

