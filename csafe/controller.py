from threading import Lock

from .messages import *

class Controller:
    def __init__(self, transport, timeout=0.1, debug=False, get_packet_iterations=5):
        self.transport = transport
        self.debug = debug
        self.pending = b''
        self.lock = Lock()
        self.get_packet_iterations = get_packet_iterations
        self.timeout = timeout

    def packet_send(self, message):
        """ Puts data onto the line
        """
        binary_packet = message.serialize()
        self.transport.write(binary_packet)
        if self.debug:
            print(f"SEND: {binary_packet.hex()}")
            print(f"      {message.dump()}")
        return binary_packet

    def get_packet_data(self):
        """ Fetches the next packet on the line
        """
        # We read packet data until one of the following conditions
        # are met.

        # 1. We get a F2 byte to signal the end of a packet
        # 2. We time out
        while STOP_FLAG not in self.pending:
            data = self.transport.read(self.timeout)

            # no data? then we timed out
            if not data:
                if self.debug:
                    print("TIMEDOUT")
                return

            self.pending += data

        if self.debug:
            print("PENDING DATA TO PARSE:", self.pending)

        # Clip all data up to and including the STOP_FLAG
        stop_index = self.pending.index(STOP_FLAG) + 1
        packet = self.pending[:stop_index]
        self.pending = self.pending[stop_index:]

        response = MessageResponse(packet)
        if self.debug:
            print(f"RECV: {packet.hex()}")
            print(f"  dump: {response.dump()}")
        return response

    def _get_packet(self, iterations=None):
        if iterations is None:
            iterations = self.get_packet_iterations
        # We then wait for someone to enter their user ID
        loop = 0
        while True:
            data = self.get_packet_data()
            if data:
                if self.debug:
                    print(f"GOT PACKET {data} iteration {loop} ", data.dump())
                return data
            loop += 1
            if iterations and loop > iterations:
                if self.debug:
                    print(f"No packet in in {iterations} iterations")
                break
        return

    def get_packet(self):
        """ This gets the next packet and also locks the IO so that no one else
            can mess with it while we're waiting.
        """
        with self.lock:
            packet = self._get_packet(1)
        return packet

    def do_command(self, cmd, *a, **kw, _wait_response=True):
        """ Invokes a command from the COMMANDS table by generating the message
            and then sending to the peripheral. Finally will wait for the next
            message from the peripheral
        """
        m, c = MessageCommand().command(cmd, *a, **kw)
        with self.lock:
            self.packet_send(m)
            if c.spec['response'] is not None and _wait_response:
                packet = self._get_packet()
                return packet

    def get_status(self):
        """ Returns the current status of the peripheral
        """
        m, c = MessageCommand().get_status()
        with self.lock:
            self.packet_send(m)
            packet = self._get_packet()
        return packet

    def __getattr__(self, k):
        if k in COMMANDS_BY_NAME:
            return lambda *a, **kw: self.do_command(k, *a, **kw)
        raise AttributeError(f'{k} does not exist in {self}')
