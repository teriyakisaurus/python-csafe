
from .fields import *

COMMAND_HEADERS = [ 'id', 'name', 'official_name', 'command', 'response', 'description' ]

COMMANDS = [

     [ 128, 'get_status',        'cmdGetStatus',          
       [], # command
       [], # response 
       "Request Status from Slave. Status is sent even if the flgAck flag is off, i.e. this command can be added to a frame to force an acknowledgment of the frame even it the flgAck is off. Unlike the \"Empty Frame\" this command does update the Status." ],

     [ 129, 'reset',             'cmdReset',              
       [], # command
       [], # response 
       "Reset Slave. Initialize variables to Ready State and reset Frame Toggle and Status of Previous Frame flag to zero." ],

     [ 130, 'go_idle',           'cmdGoIdle',             
       [], # command
       [], # response 
       "go to Idle State, reset variables to Idle state" ],

     [ 131, 'go_haveid',         'cmdGoHaveID',           
       [], # command
       [], # response 
       "go to HaveID state" ],

     [ 133, 'go_inuse',          'cmdGoInUse',            
       [], # command
       [], # response 
       "go to InUse State" ],

     [ 134, 'go_finished',       'cmdGoFinished',         
       [], # command
       [], # response 
       "go to Finished State" ],

     [ 135, 'go_ready',          'cmdGoReady',            
       [], # command
       [], # response 
       "go to Ready State" ],

     [ 136, 'badid',             'cmdBadID',              
       [], # command
       [], # response 
       "Indicates to Slave that the user ID entered was invalid" ],

     [   1, 'autoupload',        'cmdAutoUpload',         
       [ BYTES('bytes')], # command
       [ BYTES('bytes')], # response 
       "Control automatic upload features. See table below for definition." ],

     [   2, 'uplist',            'cmdUpList',             
       [ STR('misc') ], # command
       [ STR('misc') ], # response 
       "List of commands that specify data for batched upload. *" ],

     [   4, 'upstatussec',       'cmdUpStatusSec',        
       [ BYTES('bytes')], # command
       [ BYTES('bytes')], # response 
       "Interval between periodic automatic uploads of status **" ],

     [   5, 'uplistsec',         'cmdUpListSec',          
       [ BYTES('bytes')], # command
       [ BYTES('bytes')], # response 
       "Interval between periodic automatic uploads of UpList **" ],

     [  16, 'iddigits',          'cmdIDDigits',           
       [ BYTES('bytes')], # command
       [ BYTES('bytes')], # response 
       "An integer between 2 - 5 defining the number of digits to accept from the user as a valid ID" ],

     [  17, 'set_time',          'cmdSetTIme',            
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Set current time of day" ],

     [  18, 'set_date',          'cmdSetDate',            
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Set current date." ],

     [  19, 'set_timeout',       'cmdSetTimeout',         
       [ BYTES('bytes')], # command
       [], # response
       "Set timeout period for exiting certain states. See state diagram for details." ],

     [  26, 'usercfg1',          'cmdUserCfg1',           
       [ BYTES('config') ], # command
       [ BYTES('config') ], # response 
       "Slave depended configuration information" ],

     [  27, 'usercfg2',          'cmdUserCfg2',           
       [ BYTES('config') ], # command
       [ BYTES('config') ], # response 
       "Slave depended configuration information" ],

     [  32, 'set_twork',         'cmdSetTWork',           
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Workout time goal" ],

     [  33, 'set_horizontal',    'cmdSetHorizontal',      
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Horizontal distance goal" ],

     [  34, 'set_vertical',      'cmdSetVertical',        
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Vertical distance goal" ],

     [  35, 'set_calories',      'cmdSetCalories',        
       [ BYTE('byte1'), BYTE('byte2') ], # command
       [], # response
       "Calories goal" ],

     [  36, 'set_program',       'cmdSetProgram',         
       [ BYTE('byte1'), BYTE('byte2') ], # command
       [], # response
       "Machine program and level" ],

     [  37, 'set_speed',         'cmdSetSpeed',           
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Equipment speed" ],

     [  40, 'set_grade',         'cmdSetGrade',           
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Equipment grade (incline)" ],

     [  41, 'set_gear',          'cmdSetGear',            
       [ BYTES('bytes')], # command
       [], # response
       "Equipment gear (resistance)" ],

     [  43, 'set_userinfo',      'cmdSetUserInfo',        
       [ STR('info') ], # command
       [], # response
       "General user information" ],

     [  44, 'set_torque',        'cmdSetTorque',          
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Equipment torque" ],

     [  45, 'set_level',         'cmdSetLevel',           
       [ BYTES('bytes')], # command
       [], # response
       "Level (bike=power, stepper=speed)" ],

     [  48, 'set_targethr',      'cmdSetTargetHR',        
       [ BYTES('bytes')], # command
       [], # response
       "Target HR (bpm)" ],

     [  50, 'set_goal',          'cmdSetGoal',            
       [ BYTES('goal') ], # command
       [], # response
       "Sets a workout goal01HTime in HR Zone" ],

     [  51, 'set_mets',          'cmdSetMETS',            
       [ BYTE('byte1'), BYTE('byte2') ], # command
       [], # response
       "METS goal" ],

     [  52, 'set_power',         'cmdSetPower',           
       [ INT('value'), UNIT('unit') ], # command
       [], # response
       "Power goal" ],

     [  53, 'set_hrzone',        'cmdSetHRZone',          
       [ BYTE('byte1'), BYTE('byte2') ], # command
       [], # response
       "Target HR zone (bpm)" ],

     [  54, 'set_hrmax',         'cmdSetHRMax',           
       [ BYTES('bytes')], # command
       [], # response
       "Maximum HR limit (bpm)" ],

     [  64, 'set_channelrange',  'cmdSetChannelRange',    
       [ BYTE('byte1'), BYTE('byte2') ], # command
       [], # response
       "Audio channel range (inclusive)" ],

     [  65, 'set_volumerange',   'cmdSetVolumeRange',     
       [ BYTE('byte1'), BYTE('byte2') ], # command
       [], # response
       "Audio volume range (inclusive)" ],

     [  66, 'set_audiomute',     'cmdSetAudioMute',       
       [ BYTES('bytes')], # command
       [], # response
       "Set audio muting state" ],

     [  67, 'set_audiochannel',  'cmdSetAudioChannel',    
       [ BYTES('bytes')], # command
       [], # response
       "Set audio channel" ],

     [  68, 'set_audiovolume',   'cmdSetAudioVolume',     
       [ BYTES('bytes')], # command
       [], # response
       "Set audio volume" ],

     [  96, 'starttext2,3,4,5',  'cmdStartText2,3,4,5',   
       [ STR('text') ], # command
       [ STR('text') ], # response 
       "Start text upload" ],

     [  97, 'appendtext2,3,4,5', 'cmdAppendText2,3,4,5',  
       [ BYTES('config') ], # command
       [ BYTES('config') ], # response 
       "Append text to previous cmdStartText" ],

     [ 224, 'endtext2,6',        'cmdEndText2,6',         
       [], # command
       [], # response 
       "End of text upload" ],

     [ 225, 'displaypopup7',     'cmdDisplayPopup7',      
       [], # command
       [], # response 
       "Display pop up" ],

     [ 101, 'get_textstatus',    'cmdGetTextStatus',      
       [], # command
       [ BYTES('status') ], # response 
       "Indicates the current status of a certain message type string on the slave" ],

     [ 229, 'get_popupstatus8',  'cmdGetPopupStatus8',    
       [], # command
       [], # response 
       "Indicates the status of a previous cmdDisplayPopup request" ],

     [ 145, 'get_version',       'cmdGetVersion',         
       [], # command
       [ BYTES('version') ], # response 
       "Codes used to uniquely identify equipment and ROM version.2" ],

     [ 146, 'get_id',            'cmdGetID',              
       [], # command
       [ BYTES('id') ], # response 
       "ID # defined for the user" ],

     [ 147, 'get_units',         'cmdGetUnits',           
       [], # command
       [ BYTES('bytes')], # response 
       "Unit Mode (Metric/English)" ],

     [ 148, 'get_serial',        'cmdGetSerial',          
       [], # command
       [ BYTES('config') ], # response 
       "Return equipment serial number" ],

     [ 152, 'get_list',          'cmdGetList',            
       [], # command
       [ BYTES('config') ], # response 
       "List of batched commands configured with cmdUpList.3" ],

     [ 153, 'get_utilization',   'cmdGetUtilization',     
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Hours used since manufactured" ],

     [ 154, 'get_motorcurrent',  'cmdGetMotorCurrent',    
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Motor current" ],

     [ 155, 'get_odometer',      'cmdGetOdometer',        
       [], # command
       [ STR('info') ], # response 
       "Equipment odometer value" ],

     [ 156, 'get_errorcode',     'cmdGetErrorCode',       
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Equipment error code" ],

     [ 157, 'get_servicecode',   'cmdGetServiceCode',     
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Equipment service code" ],

     [ 158, 'get_usercfg1',      'cmdGetUserCfg1',        
       [], # command
       [ BYTES('config') ], # response 
       "Slave dependent configuration data" ],

     [ 159, 'get_usercfg2',      'cmdGetUserCfg2',        
       [], # command
       [ BYTES('config') ], # response 
       "Slave dependent configuration data" ],

     [ 160, 'get_twork',         'cmdGetTWork',           
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Workout duration ***" ],

     [ 161, 'get_horizontal',    'cmdGetHorizontal',      
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Accumulated (for the workout) Distance (horizontal )" ],

     [ 162, 'get_vertical',      'cmdGetVertical',        
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Accumulated (for the workout) Distance (vertical)" ],

     [ 163, 'get_calories',      'cmdGetCalories',        
       [], # command
       [ BYTE('byte1'), BYTE('byte2') ], # response 
       "Accumulated Calories Burned" ],

     [ 164, 'get_program',       'cmdGetProgram',         
       [], # command
       [ BYTE('byte1'), BYTE('byte2') ], # response 
       "Current Machine program and level" ],

     [ 165, 'get_speed',         'cmdGetSpeed',           
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Current Speed" ],

     [ 166, 'get_pace',          'cmdGetPace',            
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Current Pace" ],

     [ 167, 'get_cadence',       'cmdGetCadence',         
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Current Cadence" ],

     [ 168, 'get_grade',         'cmdGetGrade',           
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Current grade of exercise" ],

     [ 169, 'get_gear',          'cmdGetGear',            
       [], # command
       [ BYTES('bytes')], # response 
       "Current Gear" ],

     [ 170, 'get_uplist',        'cmdGetUpList',          
       [], # command
       [ BYTES('config') ], # response 
       "Batched workout results as defined by CmdUpList **" ],

     [ 171, 'get_userinfo',      'cmdGetUserInfo',        
       [], # command
       [ STR('info') ], # response 
       "Current user information" ],

     [ 172, 'get_torque',        'cmdGetTorque',          
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Current torque" ],

     [ 176, 'get_hrcur',         'cmdGetHRCur',           
       [], # command
       [ BYTES('bytes')], # response 
       "Current HR (BPM)" ],

     [ 178, 'get_hrtzone',       'cmdGetHRTZone',         
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Current time in target HR zone" ],

     [ 179, 'get_mets',          'cmdGetMETS',            
       [], # command
       [ BYTE('byte1'), BYTE('byte2') ], # response 
       "Current 3.5 ml/kg/min oxygen consumption rating" ],

     [ 180, 'get_power',         'cmdGetPower',           
       [], # command
       [ INT('value'), UNIT('unit') ], # response 
       "Current Power expenditure ( i.e. calories/min or watts )" ],

     [ 181, 'get_hravg',         'cmdGetHRAvg',           
       [], # command
       [ BYTES('bytes')], # response 
       "Current average heart rate (bpm)" ],

     [ 182, 'get_hrmax',         'cmdGetHRMax',           
       [], # command
       [ BYTES('bytes')], # response 
       "Workout maximum heart rate (bpm)" ],

     [ 190, 'get_userdata1',     'cmdGetUserData1',       
       [], # command
       [ BYTES('config') ], # response 
       "Slave dependent workout data" ],

     [ 191, 'get_userdata2',     'cmdGetUserData2',       
       [], # command
       [ BYTES('config') ], # response 
       "Slave dependent workout data" ],

     [ 192, 'get_audiochannel',  'cmdGetAudioChannel',    
       [], # command
       [ BYTES('bytes')], # response 
       "Audio channel number selection" ],

     [ 193, 'get_audiovolume',   'cmdGetAudioVolume',     
       [], # command
       [ BYTES('bytes')], # response 
       "Audio volume setting" ],

     [ 194, 'get_audiomute',     'cmdGetAudioMute',       
       [], # command
       [ BYTES('bytes')], # response 
       "Audio muting" ],

     [ 112, 'get_caps',          'cmdGetCaps',            
       [], # command
       [ BYTES('bytes')], # response 
       "Query capabilities of slave" ],

     [ 113, 'get_info',          'cmdGetInfo',            
       [], # command
       [ BYTES('bytes')], # response 
       "Query equipment Information of slave" ],

     [ 114, 'get_errorstring',   'cmdGetErrorString',     
       [], # command
       [ STR('error') ], # response 
       "Returns a variable length printable ASCII string for associated error code." ],

]

COMMANDS_BY_ID = {}
COMMANDS_BY_NAME = {}
for c in COMMANDS:
    command = dict(zip(COMMAND_HEADERS, c))
    COMMANDS_BY_ID[command['id']] = command
    COMMANDS_BY_NAME[command['name']] = command
    COMMANDS_BY_NAME[command['official_name']] = command
    
    const_name = f"CMD_{command['official_name'][3:].upper()}"
    globals()[const_name] = command

class Command:
    __slots__ = ['spec', 'payload']

    def __init__(self, spec, payload):
        self.spec = spec
        self.payload = payload

    def __str__(self):
        c = self.spec
        return f"{c['name']}<{c['id']}> {self.payload}"

    def serialize(self):
        payload = b''
        for field in self.spec['command']:
            data = self.payload[field.name]
            payload += data.serialize()
        payload_size = len(payload)
        command_id = self.spec['id']

        # Short vs Long commands are handled differently. If
        # the 7th bit is off, the command is a short command
        if command_id & 0x80:
            buf = struct.pack('B', command_id)

        # With a long command, we must indicate the number of bytes in
        # the payload
        else:
            buf = struct.pack('BB', command_id, len(payload))
        return buf + payload

    @classmethod
    def parse(klass, command_id, data_packet):
        spec = COMMANDS_BY_ID[command_id]
        payload = {}
        for t in spec['response']:
            v, data_packet = t.unpack( data_packet )
            payload[t.name] = v
        command_obj = klass(
                            spec=spec,
                            payload=payload
                        )
        return command_obj

    def serializable(self):
        """ Returns a primitives based datastructure so that data
            can be easily serialized
        """
        data = {}
        for field in self.spec['command']:
            data = self.payload[field.name]
            data[field.name] = data.value.serializable()
        return data


    def __eq__(self, other):
        if isinstance(other, dict):
            return self.spec['id'] == other['id']
        if isinstance(other, Command):
            return id(self) == id(other)

    def __getattr__(self, k):
        if k in self.payload:
            return self.payload[k]
        object.__getattribute__(self, k)

    def __setattr__(self, k, v):
        if k in self.__slots__:
            return object.__setattr__(self, k, v)
        self.payload[k] = v
