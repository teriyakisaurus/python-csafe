flgAutoStatus = 1<<0
flgUpStatus = 1<<1
flgUpList = 1<<2
flgAck = 1<<4
flgExternControl = 1<<6

START_FLAG = 0xf1
STOP_FLAG = 0xf2

STATUSES = [
    'error',
    'ready',
    'idle',
    'haveid',
    'UNUSED',
    'inuse',
    'paused',
    'finished',
    'manual',
    'offline',
]

for i in range(len(STATUSES)):
    status_name = f"STATUS_{STATUSES[i].upper()}"
    globals()[status_name] = i


